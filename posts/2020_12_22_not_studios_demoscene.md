---
title: not studios - demoscene
tags: demoscene, graphics
---

Coding demos using C++, OpenGL and SDL2 is a good way to make cheap music videos for abstract electronic music. Although plain OpenGL may seem a little bit outdated for making demos, as impressive open source frameworks such as [raylib](https://www.raylib.com/) or [Magnum Engine](https://magnum.graphics/) may be more suited to achieve fast and better looking results, it's still a good way to understand the inner mechanisms of 3D rendering.

For instance, the [following demo](https://player.vimeo.com/video/491144508) coded for a side-project of mine seems appropriate to transcribe repetitive music made using a tape loop with a Tascam Porta 05 four-track recorder and a few synths.

Although I still don't fully master how to sequence time using SDL2, making demos using simple rendering primitives can yield to fun looking results. It's basically trial and error, you can't always expect to achieve exactly what you had in mind through code. But errors can [still be satisfactory](https://player.vimeo.com/video/491144556).

As a data scientist, I may spend my lack of spare time to code other demos using [Magnum Engine](https://magnum.graphics/), as this rendering engine seems also well suited for data visualization. Probably more stuff to come in the future !

Sources for the demos are located [on my GitLab repository](https://gitlab.com/accpnt/not/).
