---
title: Vice Magazine - The Fiction Issue 2009
tags: books, graphics
---

![vice_fiction_issue_2009_cover](https://accpnt.eu/assets/images/vice_fiction_issue_2009_cover.png)

Cover by Hollis Brown Thornton. 

Top compartment, left to right:   
Collected Works of Arthur Conan Doyle  
Tree of smoke by Denis Johnson  

Stack from bottom to top:  
The Collected Poems of Wallace Stevens  
The Paris Review #10, Fall 1955  
Enormous Changes at the Last Minute by Grace Paley  
Desolation Angels by Jack Kerouac  
On Becoming a Novelist by John Gardner  
Deadwood by Pete Dexter  
St. Augustine in 90 Minutes by Paul Strathern  
Remember by Joe Brainard  
Wuthering Heights by Emily Brontë  
The Road to Wigan Pier by George Orwell  
Mixed Reviews by Aaron Cometbus  
Sigma Chi Studs by unknown author  

Small pile continued:  
The Canterbury Tales by Geoffrey Chaucer  
Transparent Things by Vladimir Nabokov

Tray continued:  
Then Again, Maybe I Won't by Judy Blume  
Selected Poems by WH. Auden  
American Psycho by Bret Easton Ellis  
Nausea by Jean-Paul Sartre  
Told by Uncle Remus: New Stories of the Old Plantation by Joel Chandler Harris  
Occidental Mythology by Joseph Campbell  
I Ching The Book of Change  
This Is It by Alan Watts  
Shining by Stephen King  
Bulfinch's Mythology by Thomas Bulfinch  
John Dos Passos: 1919    
The Housebreaker of Shady Hill by John Cheever  
Side Effects by Woody Allen  
The Big Sleep by Raymond Chandler  

Intermediate compartment, left to right:  
Prison Pit by Johnny Ryan  
Nick's Trip by George P. Pelecanos  
Madame Bovary by Gustave Flaubert  
Richard III by William Shakespeare  
Among the Thugs by Bill Buford  
Tropic of Cancer by Henry Miller  
The Portable Graham Greene  
Hannah and Her Sisters by Woody Allen  
Clockers by Richard Price  
Naked and Other Screenplays by Mike Leigh  
Overnight to Many Distant Cities by Donald Barthelme  
Selected Short Stories by Henry James  
Selected Poems by William Carlos Williams  
Paper Doll by Jim Shepard  
The Waste Land and Other Poems by TS. Eliot  
Selected Poems of Robert Frost  
The Collected Stories by Isaac Babel  
The Treasure of the Sierra Madre by B. Traven  
Mrs. Bridge by Evan S. Connell Jr  
Dracula by Bram Stoker  
Swann's Way by Marcel Proust  
The Pleasure of the Text by Roland Barthes  
Twisted Tits by unknown author  

Stack, by bottom:  
The Morning of the Poem by James Schuyler  
Poems of the Vikings translated by Patricia Terry  
Vox by Nicholson Baker  
The Collected Shorter Poems of Kenneth Rexroth  
Car by Harry Crews  
Mark Twain on the Damned Human Race  
The Crofter and the Laird by John McPhee  
Winesburg, Ohio by Sherwood Anderson  
Miss Lonelyhearts & The Day of the Locust by Nathanael West  
ABC of Reading by Ezra Pound  
Nam by Mark Baker  
Haunted Houses by Richard Winer and Nancy Osborn  
To kill a mockingbird by Harper Lee  
Jesus’ Son by Denis Johnson

Lower compartment, left to right:  
Cool for You by Eileen Myles  
Cruddy by Lynda Barry  
The Swamp Thing by Alan Moore  
Call for the Dead by John le Carré  
Easter Parade by Richard Yates  
In the Heart of the Heart of the Country and Other Stories by William Gass  
New hope for the dead by Charles Willeford  
Mythologies by W.B. Yeats  
Roberto Bolafio: 2666 (Part 4)  
The Most of S.J. Perelman  
Pure Slaughter Value by Robert Bingham  
The Palm-Wine Drinkard by Amos Tutuola


Stack, bottom to top:  
Puppetry for Mentally Handicapped People by Caroline Astell-Burt  
Everything That Rises Must Converge by Flannery O'Connor  
How I Conquered Your Planet by John Swartzwelder  
Goodbye, Columbus by Philip Roth  
Meditations by Marcus Aurelius  
Selected Stories by Robert Walser  
Up in the Old Hotel by Joseph Mitchell  
God's Pocket by Pete Dexter  
The Snows of Kilimanjaro by Ernest Hemingway  
What We Talk About When We Talk About Love by Raymond Carver  
Three Plays by Thorton Wilder  
Heart of Darkness & The Secret Sharer by Joseph Conrad  
The Killer Inside Me by Jim Thompson  

Tray continued:  
The Wanderers by Richard Price  
Elmore Leonard's 10 Rules of Writing  
No One Belongs Here More Than You by Miranda July  
Everything Ravaged, Everything Burned by Wells Tower  
Gone: The Last Days of the New Yorker by Renata Adler  
Infinite Jest by David Foster Wallace  
