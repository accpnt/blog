---
title: "L’âge des low tech - Philippe Bihouix"
tags: bihouix, essai
---

> Cet ouvrage développe en effet la thèse, iconoclaste j'en conviens, qu'au lieu de chercher une sortie « par le haut » aux impasses environnementales et sociétales actuelles avec toujours plus d'innovation, de hautes technologies, de métiers à valeur ajoutée, de numérique, de compétitivité, de haute performance, de travail en réseau, bref, de *développement durable, de croissance verte et d'économie 2.0*, nous devons au contraire nous orienter, au plus vite et à marche forcée, vers une société essentiellement basée sur des basses technologies, sans doute plus rudes et basiques, peut-être un peu moins performantes, mais nettement plus économes en ressources et maîtrisables localement. 

Dans la continuité du rapport Meadows et des théories de la décroissance lues chez Latouche, cet ouvrage de Bihouix m'est apparu comme un complément essentiel pour mon mémoire de fin d'études. Bihouix remet en question le progrès technique (« nous misons logiquement notre avenir sur l’innovation technologique »). En énumérant les premiers usages des matériaux (les métaux ont une part belle dans cet exposé) et leur emploi dans dans l'industrie ou même dans la vie de tous les jours, il nous informe sur leur dépréciation au fil du temps. Bihouix réhabilite des usages oubliés et explique l'évolution vers un tout technologique non soutenable. 
	
Il enchaîne sur l’impossible déploiement des dispositifs énergétiques renouvelables et l’immense demande en énergie pour l’extraction des métaux. Le minerai s’appauvrît et nécessite davantage de ressources énergétiques. Il remet aussi en cause le recyclage, qui apparaît irréaliste sans une concertation sérieuse autour des procédés de fabrication. 

Bihouix présente le monde des low-tech et propose des solutions simples fondées sur le bon sens « d’avant ». Des traditions agricoles ou industrielles éprouvées par le temps pour un futur plus soutenable. De nombreuses références indirectes sont faites à Ellul et au système technicien. Il est donc question ici de renouer avec les anciens dogmes plutôt que d’en créer de nouveaux. De nombreux cas pratiques et concrets sont présentés. Cette partie m’a rappelé les préceptes de certains minimalistes: la frugalité, l’économie des moyens.  

> Une « dictature de la concentration » nous transforme en personnes spectatrices au lieu de personnes agissantes.

Mais il m’a pourtant semblé qu’il était trop jusqu’au-boutiste. Les nombreux exemples me sont parfois apparus trop extrêmes. Peut-être sont ils énoncés pour bousculer le lecteur. Bihouix achève son essai avec une postface, qui lui fait finalement dire que l'on doit évaluer intelligemment les avancées technologiques présentes.

> Le propos de *L'âge des low tech* n'est évidemment pas de condamner toute technologie et toute utilisation *high tech*, mais d'abord d'alerter sur les conséquences environnementales, sur les fausses promesses à venir de la « croissance verte », avant d'esquisser quelques principes pour un avenir plus *soutenable*, entre autres en réduisant le phénoménal gâchis actuel de précieuses ressources.

Bihouix en bon ingénieur sait que la recherche et le développement ont un but louable, mais qu’ils sont mal orientés. Cette lecture était rafraichissante, donne un éclairage pertinent sur l'état du monde actuel et a l'immense intérêt de proposer des solutions simples. 

