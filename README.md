# blog

## accpnt

Written in Rust, using Mustache as templates. Statically generated. Originally based on [knpw.rs](https://knpw.rs/) website (original sources are available [here](https://github.com/knpwrs/knpw.rs))

## Update submodule

From the top directory, type:

```git submodule update --remote --merge```

## Extra

List posts by descending size:

```find . -type f -ls | sort -r -n -k7```

