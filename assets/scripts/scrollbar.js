function scroll_indicator() {
    var win_scroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (win_scroll / height) * 100;
    document.getElementById( 'footer__progress_bar' ).style.width = scrolled + "%";
    //document.getElementById( 'footer__progress_bar' ).innerHTML = Math.round( scrolled );
}
